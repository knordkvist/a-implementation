#include "astar.h"
#include <SDL_opengl.h>
#include <iostream>
#include <cassert>
#include <algorithm>
#include <limits>

using std::cout;  
using std::endl;
//using std::priority_queue;
using std::vector;
using std::sort;
using std::make_pair;
using std::pair;
using std::map;
using std::numeric_limits;
using std::find;

AStar::AStar(int width, int height, int columns, int rows) :
	width(width),
	height(height),
	columns(columns),
	rows(rows),
	nodeWidth((float)width/columns),
	nodeHeight((float)height/rows),
	totalNodes(0),
	updatesPerSecond(2),
	start(0),
	goal(0),
	currentNode(0),
	state(0),
	gamePaused(true),
	FPSCounter(new AverageTickCounter())
{
	this->comparator = ByTotalNodeCost(this);
	this->renderData = new RenderData(this);
	SDL_Init(SDL_INIT_VIDEO);
	SDL_EnableKeyRepeat(SDL_DEFAULT_REPEAT_DELAY, SDL_DEFAULT_REPEAT_INTERVAL);
	/* Creating the surface */
	surface = SDL_SetVideoMode(this->width, this->height, 0, SDL_HWSURFACE | SDL_OPENGL | SDL_GL_DOUBLEBUFFER);
	SDL_WM_SetCaption("A* shortest path search", NULL);

	glViewport(0, 0, this->width, this->height);
	glMatrixMode(GL_PROJECTION); glLoadIdentity();
	/* (0,0) corresponds to the top left corner and (width,height) corresponds to the bottom right corner */
	glOrtho(0, this->width, this->height, 0, 0, 1);
	glMatrixMode(GL_MODELVIEW); glLoadIdentity();
	/* Dark blue background */
	glClearColor(0.0f, 0.0f, 0.2f, 1.0f);
	/* We won't be drawing in 3D ... */
	glDisable(GL_DEPTH_TEST);
}

AStar::~AStar()
{
	for(vector<Node*>::iterator it = nodes.begin(); it != nodes.end(); ++it)
	{
		delete (*it);
	}
	SDL_Quit();
}

void AStar::run()
{
	/* For polling key events */
	SDL_Event event;
	bool running = true;
	Uint32 previousUpdate = 0;
	Uint32 renderStart;

	cout << "Controls:" << endl
		<< "Select a node type and click on a square to place a node" << endl
		<< "Right click on a node to delete it" << endl
		<< "Shortcut and corresponding node type:" << endl
		<< "s\tStart node" << endl
		<< "g\tGoal node" <<  endl
		<< "t\tTerrain (impassable)" << endl
		<< "Space:\tToggle pause" << endl
		<< "r:\tRemove all nodes from the grid" << endl
		<< "i:\tIncrement search one step" << endl
		<< "Numpad + or -:\t Increase or decrease search speed" << endl
		<< "Escape\tExit game" << endl << endl;
	while(running)
	{
		/* Check for input */
		while (SDL_PollEvent(&event))
		{
			switch(event.type)
			{
				case SDL_KEYDOWN:
				{
					switch(event.key.keysym.sym)
					{
						/* Exit the game */
						case SDLK_ESCAPE:
						{
							running = false;
							break;
						}
						/* Toggle game pause */
						case SDLK_SPACE:
						{
							if((this->state & (GameState::StartPlaced | GameState::GoalPlaced)) != (GameState::StartPlaced | GameState::GoalPlaced))
							{
								cout << "Start and goal nodes need to be placed before you can unpause" << endl;
								break;
							}
							gamePaused = !gamePaused;
							break;
						}
						/* Increment search */
						case SDLK_i:
						{
							if((this->state & (GameState::StartPlaced | GameState::GoalPlaced)) != (GameState::StartPlaced | GameState::GoalPlaced))
							{
								cout << "Start and goal nodes need to be placed before you can search" << endl;
								break;
							}
							else if(state & GameState::SearchDone)
							{
								/* We're done already */
								break;
							}
							this->incrementSearch();
							break;
						}
						/* Toggle place start node */
						case SDLK_s:
						{
							state |= GameState::PlaceNode;
							nodeType = NodeType::StartNode;
							break;
						}
						/* Toggle place goal node */
						case SDLK_g:
						{
							state |= GameState::PlaceNode;
							nodeType = NodeType::GoalNode;
							break;
						}
						/* Toggle place terrain node */
						case SDLK_t:
						{
							state |= GameState::PlaceNode;
							nodeType = NodeType::Terrain;
							break;
						}
						case SDLK_r:
						{
							this->reset();
							break;
						}
						/* Increase simulation speed */
						case SDLK_KP_PLUS:
						{
							++ this->updatesPerSecond;
							cout << "Increased simulation speed. Now at " << this->updatesPerSecond << " updates/second" << endl;
							break;
						}
						/* Decrease simulation speed */
						case SDLK_KP_MINUS:
						{
							if(this->updatesPerSecond > 1)
							{
								--this->updatesPerSecond;
								cout << "Decreased simulation speed. Now at " << this->updatesPerSecond <<  " updates/second" << endl;
							}
							break;
						}
						default: break;
					}
					break;
				}
				/* Mousemotion and right/middle mouse button events are not working due to SDL bug, this is why you can't drag-delete nodes */
				case SDL_MOUSEMOTION:
				case SDL_MOUSEBUTTONDOWN:
				{
					/* Translate from click location to grid location */
					int columnClicked = (int)(event.button.x / this->nodeWidth);
					int rowClicked = (int)(event.button.y / this->nodeHeight);
					assert(columnClicked < this->columns && rowClicked < this->rows && "Trying to click node out of bounds");
					if(event.button.button == SDL_BUTTON_LEFT)
					{
						if(this->state & GameState::PlaceNode)
						{
							/* Place the node */
							placeNode(this->nodeType, columnClicked, rowClicked);
						}
						else
						{
							cout << "No node type selected" << endl;
						}
					}
					else if(event.button.button == SDL_BUTTON_RIGHT)
					{
						removeNode(columnClicked, rowClicked);
					}
					break;
				}
				case SDL_QUIT:
				{
					running = false;
					break;
				}
				default: break;
			}
		}
		/* If the game isn't paused and the search isn't already over, and if it's time to update */
		if(!gamePaused && !(state & GameState::SearchDone) && (SDL_GetTicks() - previousUpdate) * this->updatesPerSecond > 1000)
		{
			incrementSearch();
			previousUpdate = SDL_GetTicks();
		}
		renderStart = SDL_GetTicks();
		render();
#ifdef DEBUG
		cout << "fps " << 1000/FPSCounter->calcAverageTick(SDL_GetTicks() - renderStart) << endl;
#endif
	}
}

void AStar::incrementSearch()
{
	/* We have no nodes in the open list and we haven't found the goal node */
	if(open.size() == 0 && (this->state & GameState::SearchStarted))
	{
		cout << "Couldn't find a path to the goal" << endl;
		this->state |= GameState::SearchDone;
		return;
	}

	state |= GameState::SearchStarted;

	/* Get the node with the lowest cost */
	sort(open.begin(), open.end(), comparator);
	this->currentNode = nodes[open[0]];

	/* Remove the node ID from the open list */
	open.erase(open.begin());
	/* Add the node ID to closed */
	closed.push_back(currentNode->ID);
	/* Check if it was the goal node */
	if(currentNode->ID == goal->ID)
	{
		cout << "A path was found" << endl;
		this->state |= GameState::SearchDone;
		this->state |= GameState::GoalFound;
		buildPath();
		return;
	}

	/* There is a square to the left */
	bool left = currentNode->column > 0;
	/* There is a square to the right */
	bool right = currentNode->column < this->columns-1;
	/* There is a square above */
	bool above = currentNode->row > 0;
	/* There is a square below */
	bool below = currentNode->row < this->rows-1;

	/* If there is a square above and to the left */
	if(left && above)
	{
		evaluateNode(currentNode->column-1, currentNode->row-1, AStar::diagonalCost, 1, 1);
	}
	if(above)
	{
		evaluateNode(currentNode->column, currentNode->row-1, AStar::orthogonalCost, 0, 1);
	}
	if(right && above)
	{
		evaluateNode(currentNode->column+1, currentNode->row-1, AStar::diagonalCost, -1, 1);
	}
	if(left)
	{
		evaluateNode(currentNode->column-1, currentNode->row, AStar::orthogonalCost, 1, 0);
	}
	if(right)
	{
		evaluateNode(currentNode->column+1, currentNode->row, AStar::orthogonalCost, -1, 0);
	}
	if(left && below)
	{
		evaluateNode(currentNode->column-1, currentNode->row+1, AStar::diagonalCost, 1, -1);
	}
	if(below)
	{
		evaluateNode(currentNode->column, currentNode->row+1, AStar::orthogonalCost, 0, -1);
	}
	if(right && below)
	{
		evaluateNode(currentNode->column+1, currentNode->row+1, AStar::diagonalCost, -1, -1);
	}
}

void AStar::evaluateNode(const int column, const int row, const int movementCost, int parentDirectionX, int parentDirectionY)
{
	/* Look for the node it in our grid-location-to-id map */
	map<pair<int, int>, int>::const_iterator locationToIDMapIt = locationToIDMap.find(make_pair(column, row));
	/* If we found the node we were looking for */
	if(locationToIDMapIt != locationToIDMap.end())
	{
		Node* node = nodes[locationToIDMapIt->second];
		/* If the node is walkable and its ID is not in the closed list */
		if(node->fCost < numeric_limits<int>::max() && find(closed.begin(), closed.end(), node->ID) == closed.end())
		{
			/* Search for the node in the open list */
			vector<int>::const_iterator openIt = find(open.begin(), open.end(), node->ID);
			/* If it's in the open list already */
			if(openIt != open.end())
			{
				/* The cost to go from currentNode to the node being evaluated */
				int newPathCost = this->currentNode->gCost + movementCost;
				/* If moving through currentNode to get to this node is cheaper */
				if(newPathCost < node->gCost)
				{
					/* Update this node */
					node->parent = this->currentNode;
					node->parentDirectionX = parentDirectionX;
					node->parentDirectionY = parentDirectionY;
					node->gCost = newPathCost;
					node->fCost = node->gCost + node->hCost;
				}
			}
			/* We found a node that has been created, but isn't in the closed, unwalkable or open list... It is the goal node */
			else
			{
				open.push_back(goal->ID);
				goal->parent = currentNode;
				goal->parentDirectionX = parentDirectionX;
				goal->parentDirectionY = parentDirectionY;
			}
		}
	}
	/* The node doesn't exist yet, create it */
	else
	{
		Node* newNode = new Node(totalNodes, column, row, this->currentNode->gCost + movementCost, hCost(column, row), currentNode, parentDirectionX, parentDirectionY);
		++ totalNodes;
		nodes.push_back(newNode);
		locationToIDMap.insert(make_pair(make_pair(newNode->column, newNode->row), newNode->ID));
		open.push_back(newNode->ID);
	}
}

int AStar::hCost(const int column, const int row) const
{
	int xDistance = abs(column - goal->column);
	int yDistance = abs(row - goal->row);

	if(xDistance > yDistance)
	{
		return (14 * yDistance) + (10 * (xDistance-yDistance));
	}
	else
	{
		return (14 * xDistance) + (10 * (yDistance-xDistance));
	}
}

void AStar::render() const
{
	glClear(GL_COLOR_BUFFER_BIT);

	/* Begin GL_QUADS */
	glBegin(GL_QUADS);
	if(start != 0)
	{
		/* The start node is green */
		glColor3f(0, 0.8f, 0);
		renderSquare(start->ID);
	}
	if(goal != 0)
	{
		/* The goal node is red */
		glColor3f(0.8f, 0, 0);
		renderSquare(goal->ID);
	}
	/* Render the terrain  */
	glColor3f(0.8f, 0.6f, 0.3f);
	for(vector<int>::const_iterator it = terrain.begin(); it != terrain.end(); ++it)
	{
		renderSquare(*it);
	}
	/* Render the path from start to goal */
	glColor3f(0.0f, 0.8f, 0.0f);
	if(this->state & GameState::GoalFound)
	{
		for(vector<int>::const_iterator it = finalPath.begin(); it != finalPath.end(); ++it)
		{
			renderSquare(*it);
		}
	}
	/* End GL_QUADS */
	glEnd();

	/* Render the grid */
	renderGrid();

	/* Draw frames around the nodes in the open list */
	glColor3f(1.0f, 1.0f, 1.0f);
	for(std::vector<int>::const_iterator it = open.begin(); it != open.end(); ++it)
	{
		renderFrame(*it);
	}

	/* Draw frames around the nodes in the closed list */
	glColor3f(0.0f, 0.8f, 0.0f);
	for(std::vector<int>::const_iterator it = closed.begin(); it != closed.end(); ++it)
	{
		renderFrame(*it);
	}

	if(currentNode != 0)
	{
		/* Render a frame around the current node */
		glColor3f(0.8f, 0.0f, 0.0f);
		renderFrame(currentNode->ID);
	}

	/* Render parent pointers */
	glColor3f(1.0f, 1.0f, 1.0f);
	for(std::vector<Node*>::const_iterator it = nodes.begin(); it != nodes.end(); ++it)
	{
		renderParentpointer(*it);
	}

	SDL_GL_SwapBuffers();
}

void inline AStar::renderSquare(const int nodeIndex) const
{	
	GLfloat xOffset = nodes[nodeIndex]->column * this->nodeWidth;
	GLfloat yOffset = nodes[nodeIndex]->row * this->nodeHeight;
	/* Top left */
	glVertex2f(xOffset, yOffset);
	/* Bottom left */
	glVertex2f(xOffset, yOffset + this->nodeHeight);
	/* Bottom right */
	glVertex2f(xOffset + this->nodeWidth, yOffset + this->nodeHeight);
	/* Top Right */
	glVertex2f(xOffset + this->nodeWidth, yOffset);
}

void inline AStar::renderFrame(const int nodeIndex) const
{	
	Node* node = nodes[nodeIndex];
	GLfloat top = (node->row * this->nodeHeight) + 1.0f;
	GLfloat bottom = (node->row * this->nodeHeight) + this->nodeHeight - 1.0f;
	GLfloat left = (node->column * this->nodeWidth) + 1.0f;
	GLfloat right = (node->column * this->nodeWidth) + this->nodeWidth - 1.0f;

	glBegin(GL_LINE_LOOP);
	/* Top left ... */
	glVertex2f(left, top);
	/* to bottom left ... */
	glVertex2f(left, bottom + 1);
	/* to bottom right ... */
	glVertex2f(right, bottom);
	/* to top right... */
	glVertex2f(right, top);
	glEnd();
}

void inline AStar::renderGrid() const
{
	/* Begin GL_LINES */
	glBegin(GL_LINES);
	/* The grid lines are blue */
	glColor3f(0.0f, 0.0f, 0.8f);
	/* Draw the grid's vertical lines */
	for(size_t column = 0; column < (unsigned)this->columns; ++column)
	{
		/* Top */
		glVertex2f(column * this->nodeWidth, 0.0f);
		/* Bottom */
		glVertex2f(column * this->nodeWidth, (float)this->width);
	}
	/* Draw the grid's horizontal lines */
	for(size_t row = 0; row < (unsigned)this->rows; ++row)
	{
		/* Left */
		glVertex2f(0.0f, row * this->nodeHeight);
		/* Right */
		glVertex2f((float)this->width, row * this->nodeHeight);
	}
	/* End GL_LINES */
	glEnd();
}

void inline AStar::renderParentpointer(const Node* node) const
{
	if(node->parent == 0)
	{
		return;
	}

	float midX = (node->column * this->nodeWidth) + this->nodeWidth / 2;
	float midY = (node->row * this->nodeHeight) + this->nodeHeight / 2;
	
	float lineEndX = midX + (node->parentDirectionX * this->nodeWidth / 4);
	float lineEndY = midY + (node->parentDirectionY * this->nodeHeight / 4);

	/* Draw a circle in the middle, scale it based on width and height */
	glBegin(GL_LINE_STRIP);
	for(size_t i = 0; i < renderData->circleVerticeCount * 2; i += 2)
	{
		glVertex2f(renderData->circleVertices[i] + midX, renderData->circleVertices[i+1] + midY);
	}
	glEnd();
	/* Draw a line towards the parent */
	glBegin(GL_LINES);
	glVertex2f(midX, midY);
	glVertex2f(lineEndX, lineEndY);
	glEnd();
}
	
AStar::Node::Node(int ID, int column, int row, int gCost, int hCost, Node* parent, int parentDirectionX, int parentDirectionY) :
	ID(ID),
	parent(parent),
	parentDirectionX(parentDirectionX),
	parentDirectionY(parentDirectionY),
	column(column),
	row(row),
	gCost(gCost),
	hCost(hCost),
	fCost(gCost+hCost)
{
}

void AStar::placeNode(NodeType::Node nodeType, int column, int row)
{
	if(this->state & GameState::SearchStarted)
	{
		cout << "Can't place a node when the search has started" << endl;
		return;
	}
	/* Look for the node it in our grid-location-to-id map */
	map<pair<int, int>, int>::iterator locationToIDMapIt = locationToIDMap.find(make_pair(column, row));
	if(locationToIDMapIt != locationToIDMap.end())
	{
		/* There is already a node there */
		return;
	}

	/* Update node position and grid-location-to-id map if we are placing the start node again ...*/
	if(nodeType == NodeType::StartNode && start != 0)
	{
		locationToIDMapIt = locationToIDMap.find(make_pair(start->column, start->row));
		locationToIDMap.erase(locationToIDMapIt);
		start->column = column;
		start->row = row;
		locationToIDMap.insert(make_pair(make_pair(start->column, start->row), start->ID));
		return;
	}
	/* ... this also applies to the goal node*/
	else if(nodeType == NodeType::GoalNode && goal != 0)
	{
		locationToIDMapIt = locationToIDMap.find(make_pair(goal->column, goal->row));
		locationToIDMap.erase(locationToIDMapIt);
		goal->column = column;
		goal->row = row;
		locationToIDMap.insert(make_pair(make_pair(goal->column, goal->row), goal->ID));
		return;
	}
	else
	{
		Node* newNode = new Node(totalNodes, column, row);
		++ this->totalNodes;

		if(nodeType == NodeType::Terrain)
		{
			newNode->fCost = numeric_limits<int>::max();
			newNode->gCost = numeric_limits<int>::max();
			newNode->hCost = numeric_limits<int>::max();
			this->terrain.push_back(newNode->ID);
		}
		
		this->nodes.push_back(newNode);
		locationToIDMap.insert(make_pair(make_pair(newNode->column, newNode->row), newNode->ID));

		if(nodeType == NodeType::StartNode)
		{
			this->start = newNode;
			open.push_back(newNode->ID);
			state |= GameState::StartPlaced;
			cout << "Start node created" << endl;
		}
		else if(nodeType == NodeType::GoalNode)
		{
			this->goal = newNode;
			state |= GameState::GoalPlaced;
			cout << "Goal node created" << endl;
		}
	}
}

void AStar::removeNode(int column, int row)
{
	if(this->state & GameState::SearchStarted)
	{
		cout << "Can't remove a node when the search has started" << endl;
		return;
	}

	map<pair<int, int>, int>::iterator locationToIDMapIt = locationToIDMap.find(make_pair(column, row));

	if(locationToIDMapIt == this->locationToIDMap.end())
	{
		cout << "No node to remove" << endl;
		return;
	}

	/* This is the node that will be removed */
	Node* remove = nodes[(*locationToIDMapIt).second];
	this->locationToIDMap.erase(locationToIDMapIt);
	-- this->totalNodes;

	if(remove == start)
	{
		start = 0;
		open.clear();
		this->state &= ~GameState::StartPlaced;
	}
	else if(remove == goal)
	{
		goal = 0;
		this->state &= ~GameState::GoalPlaced;
	}
	/* Terrain */
	else
	{
		this->terrain.erase(find(terrain.begin(), terrain.end(), remove->ID));
	}

	/* Since we are going to remove a node from nodes, we need to adjust all IDs after the removed element so that indexing won't be broken */
	vector<Node*>::iterator removedIt = this->nodes.erase(nodes.begin() + remove->ID);
	for(; removedIt != nodes.end(); ++removedIt)
	{
		Node* update = *removedIt;
		int newID = update->ID-1;
		/*
		 * In case the ID is present in the terrain list, we need to update it there.
		 * If we didn't do this an ID in the terrain list could point to where the terrain node _used to be_ in the node list!
		 */
		vector<int>::iterator terrainIt = find(terrain.begin(), terrain.end(), update->ID);
		if(terrainIt != terrain.end())
		{
			(*terrainIt) = newID;
		}

		/* The above is always true for the location-to-id-map, so we remove the old ID value so that we can update it */
		map<pair<int,int>, int>::iterator locationIt = locationToIDMap.find(make_pair(update->column, update->row));
		locationToIDMap.erase(locationIt);
		locationToIDMap.insert(make_pair(make_pair(update->column, update->row), newID));

		/* If it's the start node we've got to update the open list too */
		if(update == start)
		{
			open[0] = newID;
		}

		update->ID = newID;
	}
	/* Finally free the memory allocated for the removed node */
	delete remove;
}

void AStar::buildPath()
{
	assert(this->state & GameState::SearchDone && "A path cannot be built when the search isn't finished");
	
	Node* node = goal;
	while(node->parent != 0)
	{
		this->finalPath.push_back(node->ID);
		node = node->parent;
	}
}

void AStar::reset()
{
	for(vector<Node*>::const_iterator it = nodes.begin(); it != nodes.end(); ++it)
	{
		delete (*it);
	}
	nodes.clear();
	totalNodes = 0;
	currentNode = 0;
	goal = 0;
	start = 0;
	state = 0;

	open.clear();
	closed.clear();
	terrain.clear();
	finalPath.clear();
	locationToIDMap.clear();
	gamePaused = true;
}