#pragma once
#include <SDL.h>
//#include <queue>
#include <vector>
#include <map>
#include <utility>

/*
 * Implementation of the A* search algorithm using SDL + OpenGL
 */
class AStar
{
	public:
		AStar(int width = 600, int height = 600, int columns = 15, int rows = 15);
		~AStar();
		/* Updates the game and responds to keyboard input, and renders the grid */
		void run();

		/* Simple representation of game state */
		struct GameState
		{
			enum State
			{
				/* Indicates that the user wants to place a node */
				PlaceNode = 1,
				StartPlaced = 2,
				GoalPlaced = 4,
				QuickSearch = 8,
				SearchStarted = 16,
				SearchDone = 32,
				GoalFound = 64
			};
		};

		/* The type of nodes that can be placed */
		struct NodeType
		{
			enum Node
			{
				StartNode = 1,
				GoalNode = 2,
				Terrain = 4
			};
		};
	private:
		/* Contains the data associated with each node */
		struct Node
		{
			Node(int ID, int column, int row, int gCost = 0, int hCost = 0, Node* parent = 0, int parentDirectionX = 0, int parentDirectionY = 0);
			/* The node ID, used as an index to nodes */
			int ID;
			Node* parent;
			/* -1: parent to the left; 0: same column; +1: parent to the right */
			int parentDirectionX;
			/* -1: parent above; 0: same row; +1: parent below */
			int parentDirectionY;
			/* Max integer value means impassible terrain */
			int fCost;
			int gCost;
			int hCost;
			/* The node's x-location on the grid */
			int column;
			/* The node's y-location on the grid */
			int row;
		};

		struct AverageTickCounter
		{
			static const int maxSamples = 100;
			int tickIndex;
			int tickSum;
			int ticklist[maxSamples];

			/* Returns average ticks per frame over the maxSamples last frames*/
			double calcAverageTick(int newTick)
			{
				/* Subtract value falling off */
				tickSum-=ticklist[tickIndex];
				/* Add new value */
				tickSum+=newTick;
				/* Save new value so it can be subtracted later */
				ticklist[tickIndex]=newTick;
				/* Increment buffer index */
				if(++tickIndex==maxSamples)
					tickIndex=0;
				/* Return average */
				return((double)tickSum/maxSamples);
			}
		} * FPSCounter;

		struct RenderData
		{
			static const int circleVerticeCount = 10;
			/* The vertices for one circle, this data will be copied and translated to fit each parent */
			float circleVertices[circleVerticeCount * 2];
			/* The vertices for all the circles that will be rendered */
			std::vector<float> parentCircleVertices;
			float* gridVerticesHorizontal;
			float* gridVerticesVertical;

			RenderData(const AStar* instance)
			{
				const float radius = (instance->nodeWidth < instance->nodeHeight) ? instance->nodeWidth/10 : instance->nodeHeight/10;
				const float pi = 3.14159265f;
				const float degreeToRadian = pi / 180;

				/* One vertex will be used to complete the circle */
				const float parentCircleStepSize = 360 / (circleVerticeCount - 1);
				for(size_t i = 0; i < circleVerticeCount * 2; i += 2)
				{	
					float radians = i/2 * parentCircleStepSize * degreeToRadian;
					circleVertices[i] = cos(radians) * radius;
					circleVertices[i+1] = sin(radians) * radius;
				}

				gridVerticesHorizontal = new float[instance->columns * 2];
				gridVerticesVertical = new float[instance->rows * 2];
				/* TODO: compute grid vertices */
			}

			//void nodeAdded(int row, int column, bool hasParent, NodeType nodeType)
			//{

			//}

			//void nodeRemoved(int row, int column, bool hasParent, NodeType nodeType)
			//{

			//}

			//void nodeChanged(int row, int column, bool hasParent, NodeType nodeType)
			//{

			//}
		} * renderData;

		/* Comparator class used when sorting a collection of node IDs by the cost of their corresponding nodes */
		struct ByTotalNodeCost
		{
			ByTotalNodeCost() {};
			ByTotalNodeCost(const AStar* instance) : instance(instance){}
			const AStar* instance;
			inline bool operator()(const int& lhs, const int& rhs) const
			{
				return instance->nodes[lhs]->fCost < instance->nodes[rhs]->fCost;
			}
		} comparator; /* The comparator object used when sorting the open list */

		Node* start;

		Node* goal;

		/* The node currently being evaluated */
		Node* currentNode;

		/* Application dimensions */
		int width, height;

		/* Grid size */
		int columns, rows;

		/* The number of nodes created */
		int totalNodes;

		/* Stores state flags */
		int state;

		/* The node to be placed */
		NodeType::Node nodeType;

		/* This controls how often we perform each search step when the game is unpaused */
		int updatesPerSecond;

		/* The cost for moving vertically/horizontally and diagonally */
		static const int orthogonalCost = 10, diagonalCost = 14;

		/* Node dimensions */
		float nodeWidth, nodeHeight;

		bool gamePaused;

		/* Contains all nodes created */
		std::vector<Node*> nodes;

		/* Keeps track of the nodes which are about to be evaluted */
		std::vector<int> open;

		/* Contains the IDs of the nodes we are done processing */
		std::vector<int> closed;

		/* Contains the ID of the terrain nodes */
		std::vector<int> terrain;

		/* Contains the path from start to goal */
		std::vector<int> finalPath;

		/* Maps grid locations to node index */
		std::map<std::pair<int, int>, int> locationToIDMap;

		/* Performs one search step */
		void incrementSearch();

		/* Renders the grid and its nodes */
		void render() const;

		/* Renders the supplied Node on a whole square */
		void renderSquare(const int nodeIndex) const;

		/* Renders a frame around the supplied square */
		void renderFrame(const int nodeIndex) const;

		/* Renders the grid */
		void renderGrid() const;

		/* Renders a symbol which points to the supplied node's parent */
		void inline renderParentpointer(const Node* node) const;

		/* If there is no node at (column, row) one will be created and added to the open list,
		 * the node will be ignored if it's in the closed list or unwalkable,
		 * the node will be updated if it's in the open list and the path from the current node to this node is shorter
		 */
		void evaluateNode(const int column, const int row, const int movementCost, int parentDirectionX, int parentDirectionY);

		/* Places a node on an unoccupied square on the map */
		void placeNode(NodeType::Node nodeType, int column, int row);

		/* Removes the node */
		void removeNode(int column, int row);

		/* Calculates the heuristic part of a node's cost function using the diagonal shortcut method */
		int hCost(const int column, const int row) const;

		/* Populates finalPath with the nodes that make up the path from start to goal */
		void buildPath();

		/* Removes all nodes from the grid */
		void reset();

		/* An area of graphical memory that can be written to */
		SDL_Surface* surface;
};