#include "astar.h"
#include <iostream>

int main(int argc, char* argv[])
{
	AStar* game;

	if(argc == 5)
	{
		int width = atoi(argv[1]);
		int height = atoi(argv[2]);
		int columns = atoi(argv[3]);
		int rows = atoi(argv[4]);

		if(width > 0 && height > 0 && columns > 0 && rows > 0)
		{
			game = new AStar(width, height, columns, rows);
		}
	}
	else if(argc == 1)
	{
		game = new AStar();
	}
	else
	{
		std::cout << "Usage: " << argv[0] << " <width> <height> <columns> <rows>" << std::endl;
		return 1;
	}
	game->run();
	delete game;
	return 0;
}