Program info:
This program is meant to show how A* works by incrementing the search one step at a time while rendering the open/closed lists and node parents
(and of course the final path, if it is found).
There are no bugs that I am aware of...
Place a start node, a goal node and maybe some impassable terrain, then unpause the game (search increment delay can be changed).
You can also increment the search one step at a time by pressing i.
There is a bug with the latest SDL dll which makes it impossible to to register mousemovement events and right-mouse-button-down at the same time;
this means you you have to right click one time for every node you want to delete, but you can at least create nodes by dragging (just remember to select a node type using the hotkeys).
If two terrain nodes are next to each other diagonally, then it is possible to "jump over" where the edges are connected. This is intended.

Controls:
Select a node type and click on a square to place a node
Right click on a node to delete it
Shortcut and corresponding node type:
 s	Start node
 g	Goal node
 t	Terrain (impassable)
Space: Toggle pause
r: Remove all nodes from the grid
i: Increment search one step
Numpad + or -: Increase or decrease search speed
Escape: Exit game